// References
const wordEl = $e("word");
const wrongLettersEl = $e("wrong-letters");
const playAgainBtn = $e("play-button");
const popup = $e("popup-container");
const notification = $e("notification-container");
const finalMessage = $e("final-message");
const finalMessageRevealWord = $e("final-message-reveal-word");

const figureParts = document.querySelectorAll(".figure-part");

const numberOfGuesses = figureParts.count;

const words = ["great", "willson", "awesomeAgain", "awesome"];

let selectedWord = words[Math.floor(Math.random() * words.length)];

let playable = true;

const correctLetters = [];
var wrongLetters = [];
var displayWordString = "";

var wordArray = selectedWord.split("");
console.log(wordArray);

window.onload = wordArray.forEach((letter, i) => {
  letter = document.createElement("li");
  letter.classList.add("letter");
  letter.id = "letter" + i;
  wordEl.appendChild(letter);
});

//Set variable includesLetter to a boolean that checks if the wordArray has the letter entered
//If it does include the letter start with the variable found
//found will check the wordArray for the first letter that matches the letter entered. It will not find multiples, only the very first occurance
//That letter will be pushed to the correctLetters array
//Run function displayCorrectLetters(letter) where letter is the keydown event
//If the wordArray does not include the letter entered start by pushing the letter into the wrongLetters array
//Run both displayFigure() and displayWrongLetters() functions
function checkGuess(letter) {
  let includesLetter = wordArray.includes(letter);
  if (includesLetter) {
    let found = wordArray.find(element => element === letter);
    correctLetters.push(found);
    displayCorrectLetters(letter);
  } else {
    wrongLetters.push(letter);
    displayFigure();
    displayWrongLetters();
  }
}

function displayWrongLetters() {
  wrongLettersEl.innerHTML = `${wrongLetters
    .map(
      letter => `
		<span class="wrongLetter">
			${letter}
		</span> `
    )
    .join("")}`;
}

//Var blanks will target the blank spaces for the selectedWord
//displayCorrectLetters(letter) starts with a for loop that iterates through the blank spaces
//If the letter entered is equal to the letter in the wordArray at the given index and
//If the blank space is empty append the letter entered
var blanks = document.getElementsByClassName("letter");
function displayCorrectLetters(letter) {
  for (let i = 0; i < blanks.length; i++) {
    if (wordArray[i].toLowerCase() == letter) {
      if (blanks[i].innerHTML == "") {
        blanks[i].append(letter);
      }
    }
  }
}

function displayFigure() {
  for (count = 0; count < wrongLetters.length; count++) {
    figureParts[count].removeAttribute("class");
  }
}

//Event for when someone presses a letter
window.addEventListener("keydown", e => {
  if (playable) {
    if (e.keyCode >= 65 && e.keyCode <= 90) {
      const letter = e.key.toLowerCase();
      checkGuess(letter);
    }
  }
});

//ez mode for declare
function $e(id) {
  return document.getElementById(id);
}

message();

function message() {
  if (correctLetters.length === selectedWord.length) {
    popup.style.display = "flex";
    finalMessage.innerHTML = "You guessed correctly, homie!";
  }

  if (wrongLetters.length === figureParts.length) {
    popup.style.display = "flex";
    finalMessage.innerHTML = "You lost sucka!";
    finalMessageRevealWord.innerHTML =
      'The winning word was "' + selectedWord + '".';
    playable = false;
  }
}
